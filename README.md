-- The base url is configured as http://localhost/robotix. 
-- Put the files in a folder called robotix inside localhost. 
-- To change base url settings change the settings in the file -> applications/config/config.php 
-- To change the database settings change applications/config/database.php
-- Current settings - Database location - localhost User - root Password - <none> Database name - rbtx13
-- For Linux only: Site will work using url if everything else is fine and every time you will have to add /index.php to get correct link: http:// localhost/robotix/index.php/home/page/welcome This is because .htaccess file is not enabled byu default on linux. To enable: -- sudo gedit /etc/apache2/sites-available/defaultD -- Under all the necessary directories change AllowOverride None to AllowOverride All -- Enable rewrite: sudo a2enmod rewrite -- Restart server and test with usual url. It should work. :)